package com.jarsilio.android.common.logging

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.util.Log
import androidx.core.content.FileProvider
import com.jarsilio.android.common.extensions.appVersionName
import com.jarsilio.android.common.extensions.applicationId
import com.jarsilio.android.common.extensions.defaultEmail
import com.jarsilio.android.common.extensions.logFile
import com.jarsilio.android.common.extensions.logFiles
import com.jarsilio.android.common.extensions.logsDir
import com.jarsilio.android.common.extensions.mergedLogFile
import com.jarsilio.android.common.extensions.mergedLogFileUri
import com.jarsilio.android.common.extensions.rotatedLogFile
import java.io.BufferedReader
import java.io.BufferedWriter
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import timber.log.Timber

const val MAX_TAG_LENGTH = 23
const val MAX_LOG_FILE_SIZE = 500 * 1024

class LogProvider : FileProvider()

data class LogData(val priority: Int, val tag: String?, val message: String, val t: Throwable? = null)

@Suppress("unused")
class PersistentTree(private val context: Context) : Timber.DebugTree() {

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        LogWriterAsyncTask(context).execute(LogData(priority, tag, message))
    }
}

class LogWriterAsyncTask(private val context: Context) : AsyncTask<LogData, Void?, Void?>() {

    private val logUtils: LogUtils by lazy { LogUtils(context) }

    @SuppressLint("SimpleDateFormat")
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS zzz")

    override fun doInBackground(vararg params: LogData): Void? {
        val logData = params[0]

        val now = dateFormat.format(Date())

        val priorityString = when (logData.priority) {
            Log.VERBOSE -> "V"
            Log.DEBUG -> "D"
            Log.INFO -> "I"
            Log.WARN -> "W"
            Log.ERROR -> "E"
            Log.ASSERT -> "A"
            else -> "U"
        }

        try {
            logUtils.createLogsDirIfNecessary()
            logUtils.rotateLogsIfNecessary()
            context.logFile.appendText("$now: $priorityString ${logData.tag}: ${logData.message}\n")
        } catch (e: IOException) {
            Log.e("LogWriterAsyncTask", "Failed to log to cache dir")
        }
        return null
    }
}

class LogSenderAsyncTask(private val context: Context, private val issue: String) : AsyncTask<Void?, Void?, Void?>() {
    /*
     * Copyright (c) 2017 ACRA Team (acra.ch)
     * Copyright (c) 2019 Juan García Basilio
     *
     * Licensed under the Apache License, Version 2.0 (the "License");
     * you may not use this file except in compliance with the License.
     * You may obtain a copy of the License at
     *
     *     http://www.apache.org/licenses/LICENSE-2.0
     *
     * Unless required by applicable law or agreed to in writing, software
     * distributed under the License is distributed on an "AS IS" BASIS,
     * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
     * See the License for the specific language governing permissions and
     * limitations under the License.
     */

    override fun doInBackground(vararg params: Void?): Void? {
        mergeRotatedLogs()
        return null
    }

    override fun onPostExecute(result: Void?) {
        super.onPostExecute(result)
        sendEmail(issue)
    }

    private fun sendEmail(issue: String?) {
        val subject = "${context.applicationId} (version ${context.appVersionName}) Debug Logs"
        val messageBody = "Dear Dev," +
                "\n\nWould you be so kind to take a look at the attached logs?" +
                "\n\n$issue" +
                "\n\nThank you very much!"

        val attachments = arrayListOf(context.mergedLogFileUri)
        val attachmentIntent = buildAttachmentIntent(subject, messageBody, attachments)
        val resolveIntent = buildResolveIntent(subject, messageBody)
        val resolveActivity = resolveIntent.resolveActivity(context.packageManager)
        val initialIntents = buildInitialIntents(context.packageManager, resolveIntent, attachmentIntent)
        val packageName = getPackageName(resolveActivity, initialIntents)
        attachmentIntent.setPackage(packageName)

        if (packageName == null) {
            Timber.d("Letting user choose email client")
            for (intent in initialIntents) {
                val `package` = intent.`package`
                if (`package` != null) {
                    grantPermission(context, intent, `package`, attachments)
                }
            }
            showChooser(context, initialIntents)
        } else if (attachmentIntent.resolveActivity(context.packageManager) != null) {
            Timber.d("Using default email client")
            grantPermission(context, attachmentIntent, packageName, attachments)
            context.startActivity(attachmentIntent)
        } else {
            Timber.w("No email client supporting attachments found. Attachments will be ignored")
            context.startActivity(resolveIntent)
        }
    }

    private fun mergeRotatedLogs() {
        BufferedWriter(FileWriter(context.mergedLogFile)).use { writer ->
            for (logFile in context.logFiles) {
                if (logFile.exists()) {
                    BufferedReader(FileReader(logFile)).use { reader ->
                        reader.forEachLine { writer.write("$it\n") }
                    }
                }
            }
        }
    }

    private fun buildInitialIntents(pm: PackageManager, resolveIntent: Intent, emailIntent: Intent): MutableList<Intent> {
        val resolveInfoList = pm.queryIntentActivities(resolveIntent, PackageManager.MATCH_DEFAULT_ONLY)
        val initialIntents = mutableListOf<Intent>()
        for (info in resolveInfoList) {
            val packageSpecificIntent = Intent(emailIntent)
            packageSpecificIntent.setPackage(info.activityInfo.packageName)
            if (packageSpecificIntent.resolveActivity(pm) != null) {
                initialIntents.add(packageSpecificIntent)
            }
        }
        return initialIntents
    }

    private fun getPackageName(resolveActivity: ComponentName, initialIntents: List<Intent>): String? {
        var packageName: String? = resolveActivity.packageName
        if (packageName == "android") {
            // Multiple activities support the intent and no default is set
            if (initialIntents.size > 1) {
                packageName = null
            } else if (initialIntents.size == 1) {
                // Only one of them supports attachments, use that one
                packageName = initialIntents[0].getPackage()
            }
        }
        return packageName
    }

    private fun grantPermission(context: Context, intent: Intent, packageName: String, attachments: List<Uri>) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        } else {
            // Flags do not work on extras prior to lollipop, so we have to grant read permissions manually
            for (uri in attachments) {
                context.grantUriPermission(packageName, uri, Intent.FLAG_GRANT_READ_URI_PERMISSION)
            }
        }
    }

    private fun buildResolveIntent(subject: String, body: String): Intent {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.fromParts("mailto", context.defaultEmail, null)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        intent.putExtra(Intent.EXTRA_TEXT, body)
        return intent
    }

    private fun showChooser(context: Context, initialIntents: MutableList<Intent>) {
        val chooser = Intent(Intent.ACTION_CHOOSER)
        chooser.putExtra(Intent.EXTRA_INTENT, initialIntents.removeAt(0))
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, initialIntents.toTypedArray())
        chooser.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        context.startActivity(chooser)
    }

    private fun buildAttachmentIntent(subject: String, body: String?, attachments: ArrayList<Uri>): Intent {
        val intent = Intent(Intent.ACTION_SEND_MULTIPLE)
        intent.putExtra(Intent.EXTRA_EMAIL, arrayOf(context.defaultEmail))
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        intent.putExtra(Intent.EXTRA_SUBJECT, subject)
        intent.type = "message/rfc822"
        intent.putParcelableArrayListExtra(Intent.EXTRA_STREAM, attachments)
        intent.putExtra(Intent.EXTRA_TEXT, body)
        return intent
    }
}

@Suppress("unused")
class LongTagTree(context: Context) : Timber.DebugTree() {
    private val packageName = context.packageName

    private fun getMessage(tag: String?, message: String): String {
        return if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            // Tag length limitation (<23): Use truncated package name and add class name to message
            "$tag: $message"
        } else {
            // No tag length limit limitation: Use package name *and* class name
            message
        }
    }

    private fun getTag(tag: String?): String {
        var newTag: String
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.M) {
            // Tag length limitation (<23): Use truncated package name and add class name to message
            newTag = packageName
            if (newTag.length > MAX_TAG_LENGTH) {
                newTag = "..." + packageName.substring(packageName.length - MAX_TAG_LENGTH + 3, packageName.length)
            }
        } else {
            // No tag length limit limitation: Use package name *and* class name
            newTag = "$packageName ($tag)"
        }

        return newTag
    }

    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
        val newMessage = getMessage(tag, message)
        val newTag = getTag(tag)

        super.log(priority, newTag, newMessage, t)
    }
}

class LogUtils(private val context: Context) {

    fun plantPersistentTreeIfNonePlanted() {
        var plantPersistentTree = true
        for (tree in Timber.forest()) {
            if (tree is PersistentTree) {
                plantPersistentTree = false
            }
        }

        if (plantPersistentTree) {
            createLogsDirIfNecessary()
            Timber.plant(PersistentTree(context))
            Timber.d("Planted a PersistentTree. Started logging to file...")
        }
    }

    fun uprootPersistentTrees() {
        Timber.d("Removing (uprooting) all planted PersistentTrees")
        for (tree in Timber.forest()) {
            if (tree is PersistentTree) {
                Timber.d("Uprooting PersistentTree (stop logging to cache directory)")
                Timber.uproot(tree)
            }
        }
    }

    fun deletePersistentLogs() {
        if (context.logsDir.exists()) {
            context.logsDir.delete()
        }
    }

    fun createLogsDirIfNecessary() {
        if (!context.logsDir.exists() && !context.logsDir.mkdirs()) {
            throw IOException("Failed to create log directory: ${context.logsDir}")
        }
    }

    fun rotateLogsIfNecessary() {
        context.logFile.createNewFile()
        if (context.logFile.length() > MAX_LOG_FILE_SIZE) {
            if (context.rotatedLogFile.exists()) {
                context.rotatedLogFile.delete()
            }
            context.logFile.renameTo(context.rotatedLogFile)
        }
    }
}
