package com.jarsilio.android.common.impressum

import android.os.Bundle
import android.text.method.LinkMovementMethod
import android.widget.LinearLayout
import android.widget.TextView
import com.jarsilio.android.common.R
import com.jarsilio.android.common.ToolbarActivity

class ImpressumActivity : ToolbarActivity() {

    override val childContentLayout: Int = R.layout.content_simple_html_abstract

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Not calling setContentView(R.layout.bla) from here. Toolbar activity already does that.

        val textView = TextView(this)
        textView.text = fromHtml(getString(R.string.impressum_activity_text))
        textView.movementMethod = LinkMovementMethod.getInstance()

        val linearLayout = findViewById<LinearLayout>(R.id.simple_html_abstract_linear_layout)
        linearLayout.addView(textView)
    }
}
