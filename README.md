# juanitobananas' libcommon Android Library

This library is probably only helpful for myself. Please feel free to use it, if you find it useful.

# Adding to your Android Studio Project

## As gradle dependency using jitpack.io

Follow these instructions: https://jitpack.io/#com.gitlab.juanitobananas/libcommon/

## As a git submodule

Add the library as a git submodule:

```
cd ~/git/my-cool-project
git submodule add https://gitlab.com/juanitobananas/libcommon.git
```

Note: You can (should) git commit that change to your original git repo now.

Add the git submodule as a module in Android Studio:

- File -> New -> Import Module...

- Select the `~/git/my-cool-project/libcommon` as the *source directory*

A `common` module should appear in the Android view under the app module.

Now you just need to add it as a gradle dependency:

- In app/build.gradle add `implementation project(path: ':common')` to the dependencies section.

And that's it!

### Some basic stuff about git submodules

- To clone your project with git, you will have to use the `--recurse-submodules` option.

- You can do a git pull/checkout/whatever *inside* the libcommon directory to update it or change to a different commit.

- If you have several submodules you'll probably already know this but you can update them all to the latest upstream commit with `git submodule update --recursive --remote`

- If your app is published to F-Droid, you will have to add a `submodules=yes` to your metadata so that F-Droid knows how to build your app.
